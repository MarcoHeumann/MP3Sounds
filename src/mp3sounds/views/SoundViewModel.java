package mp3sounds.views;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

public class SoundViewModel
{
	@FXML
	private Button							btnNext, btnNewOne, btnNewTwo, btnNewThree, btnReOne, btnReTwo, btnReThree, btnOverOne, btnOverTwo, btnOverThree;
	private MediaPlayer						songPlayer;
	private MediaPlayer						mPlayer;
	private int								songID		= 0;
	private ArrayList<String>				listSongs	= new ArrayList<String>();
	private HashMap<String, MediaPlayer>	listPlayer	= new HashMap<String, MediaPlayer>();

	public SoundViewModel()
	{
	}

	@FXML
	private void nextMusicClip()
	{
		try
		{
			songPlayer.stop();
			songPlayer.dispose();
		}
		catch (Exception e)
		{
		}
		songPlayer = new MediaPlayer(new Media(getClass().getResource("/" + listSongs.get(songID)).toExternalForm()));
		songPlayer.setCycleCount(MediaPlayer.INDEFINITE);
		songPlayer.play();
		if (songID < listSongs.size() - 1)
		{
			songID++;
		}
		else
		{
			songID = 0;
		}
	}

	/**
	 * This version just creates a new MediaPlayer each time an effect is needed.
	 * Drawback: Slower then the other implementations.
	 * 
	 * @param a
	 */
	@FXML
	private void playNewSound(ActionEvent a)
	{
		MediaPlayer m = new MediaPlayer(new Media(getClass().getResource("/" + getFileName(a)).toExternalForm()));
		m.setOnEndOfMedia(new Runnable()
		{
			public void run()
			{
				m.dispose();
			}
		});
		m.play();
	}

	/**
	 * This version reuses a mediaplayer that has been created with the same sound already.
	 * Drawback: Can not have multiple identical sounds at once.
	 * 
	 * @param a
	 */
	@FXML
	private void playReusedSounds(ActionEvent a)
	{
		listPlayer.get(getFileName(a)).seek(Duration.ZERO);
		listPlayer.get(getFileName(a)).play();
	}

	/**
	 * This version overwrites a single mediaplayer for each new sound effect.
	 * Many drawbacks like not being able to have multiple effects at once.
	 * 
	 * @param a
	 */
	@FXML
	private void playOverwrittenSounds(ActionEvent a)
	{
		try
		{
			mPlayer.stop();
			mPlayer.dispose();
		}
		catch (Exception e)
		{
		}
		mPlayer = new MediaPlayer(new Media(getClass().getResource("/" + getFileName(a)).toExternalForm()));
		mPlayer.play();
	}

	/**
	 * Gets the filename to be played from the userData attribute in FXML
	 * 
	 * @param a A button triggered this event
	 * @return
	 */
	private String getFileName(ActionEvent a)
	{
		Button temp = (Button) a.getSource();
		return temp.getUserData().toString();
	}

	public void initialize()
	{
		// fill song list
		Arrays.asList("song1.mp3", "song2.mp3").forEach(listSongs::add);
		// fill sound list
		Arrays.asList("sound1.mp3", "sound2.mp3", "sound3.mp3").forEach(s ->
		{
			listPlayer.put(s, new MediaPlayer(new Media(getClass().getResource("/" + s).toExternalForm())));
		});
		// Start first music clip
		nextMusicClip();
	}
}